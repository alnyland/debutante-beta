year, make, model, ideal price, potential sell price
1970, chevrolet, Chevelle SS, 17000, 50000
1970, chevrolet, Camaro SS, 20000, 50000
1934, ford, Coupe, 15000, 60000
2005, audi, Rs5, 25000, 70000