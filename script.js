//Andrew Nyland, 4/1/2020
var searchable_data = [];

onload = function() {
	build_index();
	console.log("Index built");
}

function determine() {
	var input = document.getElementById("searchbox"),
		filter = input.value.toLowerCase(),
		output_div = document.getElementById("output"),
		txtValue;
	
	//reset
	output_div.innerHTML = "";
	if (filter == "") {return;}
	
	for (var i=0; i<searchable_data.length; i++) {
		txtValue = searchable_data[i].toLowerCase();
		if (txtValue.indexOf(filter) > -1) {
			var output_str = "<p>" + searchable_data[i] + "</p>";
			output_div.innerHTML += output_str;
		}
	}
}

function build_index() {
	//current implementation specific hacks:
	var mesa = document.getElementById("parse_printer");
	var rows = mesa.getElementsByTagName("tr");
	searchable_data = [];
	var valid_index = 0;
	
	//loop through rows to build index
	for (var i=0; i<rows.length; i++) {
		if (rows[i].getElementsByTagName("td").length) {
			searchable_data[valid_index] = "";
			var elems = rows[i].getElementsByTagName("td");
			//loop through elements to concatenate
			for (var j=0; j<elems.length; j++) {
				searchable_data[valid_index] += elems[j].innerHTML;
			}
			valid_index++;
		}
	}
}