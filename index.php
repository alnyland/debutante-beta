<?php //Andrew Nyland, 4/1/2020 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Debutante Ltd. - Test 4/1/2020</title>
		<link type="text/css" rel="stylesheet" href="styles.css">
		<script src="script.js"></script>
		<style type="text/css">
			#searchbox {
				width: 400px;
				max-width: 100%;
			}
		</style>
	</head>
	<body>
		<h1>Debutante Ltd. - Testing v.1</h1>
		<section>
			<h2>Current data</h2>
			<p>For testing purposes, current data can be seen at <a href="data.txt">/data.txt</a>. To test parsing abilities, the following data is printed symantically correct below.</p>
			<table id="parse_printer">
				<?php
				$row = 0;
				if (($handle = fopen("data.txt", "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						$num = count($data);	//how many columns in current data
						echo "<tr>";
						for ($c=0; $c < $num; $c++) {
							echo $row ? "<td>" : "<th>";
							echo $data[$c] . "";
							echo $row ? "</td>" : "</th>";
						}
						echo "</tr>";
						$row++;
					}
					fclose($handle);
				}
				?>
			</table>
		</section>
		<section>
			<h2>Searching</h2>
			<div id="form">
				<input type="text" id="searchbox" onkeyup="determine()" placeholder="Find your dream car here..." autofocus>
				<div id="output"></div>
			</div>
		</section>
		
	</body>
</html>